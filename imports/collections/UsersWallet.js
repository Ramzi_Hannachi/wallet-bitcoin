import { Meteor } from 'meteor/meteor'
import {Mongo} from 'meteor/mongo';
import UserWallet from '../classes/UserWallet';
import Account from '../classes/Account';
import Address from '../classes/Address';
const UsersWallet = new Mongo.Collection("userswallet");

export default UsersWallet ;
