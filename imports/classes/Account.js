import { Class } from 'meteor/jagi:astronomy';

import Address from './Address';
import Exchange from '../Enumeration/Exchange';
import SendTransactionsHistory from './SendTransactionsHistory';
import bitcore from 'bitcore-lib'; //bitcore-explorers

const Account = Class.create({
  name:'Account',
  fields:
  {
    balance:{
      type:Number,
      optional:true
    },
    name:{
      type:String,
      default:function(){
        return "";
      }
    },
    HDpublicKey:{
      type:String,
      default:function(){
        return "";
      }
    },
    numAccount:{
      type:Number,
      optional:true
    },
    Address:{
      type:[Address],
      default:function(){
        return [];
      }
    },
    arrayAddress:{
      type:[String],
      default:function(){
        return [];
      }
    },
    SendTransactionsHistory:{
      type:[SendTransactionsHistory],
      default:function(){
        return [];
      }
    }
  },
  helpers:
  {
    CallHelpers_AddressInformation:function(address,addressInformation)
    {

      //return this.Address[_index].CallHelpers_AddressInformation(addressInformation)
    },
    CallHelpers_CreateAccount:function(name_account,numAccount,seed,purpose,coin_type)
    {
      this.set('name',name_account);
      this.set('numAccount',numAccount);
      this.set('HDpublicKey',generateHDpublicKey(seed,purpose,coin_type,numAccount));
      this.CallHelpers_CreateExternalInternalAddress(purpose,coin_type,numAccount,Exchange.external,'default');
    },
    CallHelpers_CreateExternalInternalAddress:function(purpose,coin_type,numAccount,exchange,tag)
    {
      var address = new Address();
      this.Address.push(address);
      var index=indexExternalInternalAddress(this.Address,exchange);
      var _address=address.CallHelpers_setAddress(this.HDpublicKey,purpose,coin_type,numAccount,exchange,index,tag);
      this.arrayAddress.push(_address);
      return _address;
    },
    CAllHelpers_GetPublicKeyFromAddress:function(address)
    {
      for (var i = 0; i < this.Address.length; i++)
      {
        if(this.Address[i].address==address)
        {
          return this.Address[i].publicKey;
        }
      }
    },
    CAllHelpers_GeneratePrivateKeyFromAddress:function(seed,address)
    {
      for (var i = 0; i < this.Address.length; i++)
      {
        if(this.Address[i].address==address)
        {
          return this.Address[i].Generate_PrivateKey(seed);
        }
      }
    },
    Account_getPaymentAddress:function()
    {
      var Address=this.Address;
      var i=Address.length-1;
      var infromations={
        address:Address[i].get('address'),
        tag:Address[i].get('tag')
      };
      while (Address[i].get('exchange')!=Exchange.external)
      {
        i--;
        infromations={
          address:Address[i].get('address'),
          tag:Address[i].get('tag')
        };
      }
      return infromations;
    }
  }
});

var generateHDpublicKey=function(SEED,purpose,coin_type,numAccount)
{
  var _seed_hash = bitcore.crypto.Hash.sha256(new Buffer(SEED));
  var HdPrivateKey= new bitcore.HDPrivateKey.fromSeed(_seed_hash , bitcore.Networks.testnet );// ne j'amais enregistrer !!!!
  var Account_HdPrivateKey = HdPrivateKey.derive(purpose,true).derive(coin_type,true).derive(numAccount,true);
  return Account_HdPrivateKey.hdPublicKey.toString();
};
/*********/
var generate_HdPrivateKey=function(seed,purpose,coin_type,numAccount)
{
  var _seed_hash = bitcore.crypto.Hash.sha256(new Buffer(seed));
  var HdPrivateKey= new bitcore.HDPrivateKey.fromSeed(_seed_hash , bitcore.Networks.testnet );// ne j'amais enregistrer !!!!
  //console.log("HdPrivateKey:",HdPrivateKey.toString());
  var Account_HdPrivateKey = HdPrivateKey.derive(purpose,true).derive(coin_type,true).derive(numAccount,true);
  return Account_HdPrivateKey.toString();
};
var indexExternalInternalAddress=function(Address,exchange)
{
  var _index=0;
  for (var i = 0; i < Address.length; i++)
  {
    if(Address[i].get('exchange')==exchange)
    {
      _index++;
    }
  }
  return _index;
};





export default Account;














/**/
