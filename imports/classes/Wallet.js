import { Class } from 'meteor/jagi:astronomy';

import Cointype from '../Enumeration/Cointype';
import Exchange from '../Enumeration/Exchange';
import Account from './Account';
import bitcore from 'bitcore-lib'; //bitcore-explorers

const Wallet = Class.create({
  name:'Wallet',
  fields:
  {
    seed:{
      type:String,
      optional:true
    },
    purpose:{
      type:Number,
      immutable:true,
      default:function(){
        return 44;
      }
    },
    coin_type:{
      type : Cointype,
      default:function(){
        return Cointype.BitcoinTestnet;
      }
    },
    accounts:{
      type:[Account],
      default:function(){
        return [];
      }
    }
  },
  helpers:
  {
    CallHelpers_AddressInformation:function(address,numAccount,addressInformation)
    {
      return this.accounts[numAccount].CallHelpers_AddressInformation(address,addressInformation)
    },
    CallHelpers_CreateExternalAddress:function(numAccount,tag)
    {
      return this.accounts[numAccount].CallHelpers_CreateExternalInternalAddress(this.purpose,this.coin_type,numAccount,Exchange.external,tag);
    },
    CallHelpers_CreateInternalAddress:function(numAccount)
    {
      return this.accounts[numAccount].CallHelpers_CreateExternalInternalAddress(this.purpose,this.coin_type,numAccount,Exchange.internal,"internal");
    },
    CallHelpers_CreateWallet:function(name_account)
    {
      /** CreateAccount **/
      var seed = this.seed;
      var purpose = this.purpose;
      var coin_type = this.coin_type;
      var account = new Account();
      var numAccount = this.accounts.push(account);
      numAccount--;
      account.CallHelpers_CreateAccount(name_account,numAccount,seed,purpose,coin_type)
      return "ok"
    },
    CallHelpers_DefinedSeed:function(seed)
    {
      this.set('seed',seed);
    },
    IsDefinedSeed()
    {
      if(this.seed)
        return true;
      else
        return false;
    }
  }
});

export default Wallet;
