import { Class } from 'meteor/jagi:astronomy';

const Utxos = Class.create({
  name:'Utxos',
  fields:
  {
    txId:String,
    outputIndex:Number,
    satoshis:Number
  },
  events: {
    beforeSave(e)
    {
      /*const utxo = e.currentTarget;
      var createdAt = utxo.createdAt;
      var createdAt = Date.now()- createdAt;
      console.log("createdAt:",createdAt);
      if(createdAt>0)
        e.stopImmediatePropagation()*/
      //console.log("beforeSave txtId:",this.txId);
      //console.log("beforeSave createdAt:",this.createdAt);
    }
  }
});
export default Utxos;
