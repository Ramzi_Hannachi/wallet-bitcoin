import { Class } from 'meteor/jagi:astronomy';
import Exchange from '../Enumeration/Exchange';
import Utxos from './Utxos';
import AddressInformation from './AddressInformation';
import bitcore from 'bitcore-lib'; //bitcore-explorers
var lupus = require('lupus');

const Address = Class.create({
  name:'Address',
  fields:
  {
    tag:{
      type:String,
      optional:true
    },
    path:{
      type:String,
      optional:true
    },
    exchange:{
      type:Exchange,
      optional:true
    },
    index:{
      type:Number,
      optional:true
    },
    publicKey:{
      type:String,
      optional:true
    },
    address:{
      type:String,
      optional:true
    },
    utxos:{
      type:[Utxos],
      optional:true
    },
    addressInformation:{
      type:AddressInformation,
      optional:true
    }
  },
  helpers:
  {
    CallHelpers_setAddress:function(HDpublicKey,purpose,coin_type,numAccount,exchange,index,tag)
    {
      this.set('tag',tag);
      this.set('path',"m/"+purpose+"'/"+coin_type+"'/"+numAccount+"'/"+exchange+"/"+index);
      this.set('index',index);
      this.set('exchange',exchange);
      this.set('publicKey',generatePublicKey(HDpublicKey,exchange,index));
      this.set('address',generateAddress(HDpublicKey,exchange,index));
      return this.get('address');
    },
    Generate_PrivateKey:function(seed)
    {
      var _seed_hash = bitcore.crypto.Hash.sha256(new Buffer(seed));
      var HdPrivateKey= new bitcore.HDPrivateKey.fromSeed(_seed_hash , bitcore.Networks.testnet );// ne j'amais enregistrer !!!!
      var privateKey = HdPrivateKey.derive(this.path).privateKey.toString();
      return privateKey;
    }
  }
});
/*
var privateKey = generatePrivateKey(HdPrivateKey,this.exchange,this.index);
return privateKey;
var generatePrivateKey=function(HdPrivateKey,exchange,index)
{
  var HdPrivateKey = new bitcore.HDPrivateKey(HdPrivateKey);
  return HdPrivateKey.derive(exchange).derive(index).privateKey.toString();
};*/
var generatePublicKey = function(HDpublicKey,exchange,index)
{
  var AccountHDpublicKey = new bitcore.HDPublicKey(HDpublicKey);
  var publicKey = AccountHDpublicKey.derive(exchange).derive(index).publicKey;
  //console.log("publicKey:",publicKey);
  //console.log("publicKey is valid?:",bitcore.PublicKey.isValid(publicKey.toString()));
  return publicKey.toString();
  //var publicKey2 = new PublicKey('02a1633cafcc01ebfb6d78e39f687a1f0995c62fc95f51ead10a02ee0be551b5dc');
};
var generateAddress = function(HDpublicKey,exchange,index)
{
  var AccountHDpublicKey = new bitcore.HDPublicKey(HDpublicKey);
  var publicKey = AccountHDpublicKey.derive(exchange).derive(index).publicKey;
  return publicKey.toAddress().toString();
};

export default Address;
