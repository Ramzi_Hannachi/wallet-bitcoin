import { Class } from 'meteor/jagi:astronomy';

import UsersWallet from '../collections/UsersWallet';
import Wallet from './Wallet';
import Account from './Account';
var lupus = require('lupus');

const UserWallet = Class.create({
  name: 'UserWallet',
  //secured:false,
  collection: UsersWallet,
  fields:
  {
    user_id:String,
    wallet: {
      type:Wallet,
      default:function(){
        return {};
      }
    }
  },
  meteorMethods:
  {
    AddressInformation:function(address,numAccount,addressInformation)
    {
      this.wallet.CallHelpers_AddressInformation(address,numAccount,addressInformation)
    },
    CreateExternalAddress:function(numAccount,tag)
    {
      if(address=this.wallet.CallHelpers_CreateExternalAddress(numAccount,tag))
      {
        console.log(address);
        return this.save();
      }
    },
    RegisterUser:function(user)
    {
      if(id = Accounts.createUser(user))
      {
        this.set('user_id',id);
        if(this.save())
          return "ok";
        else
          throw new Meteor.Error(413,"userwallet.save()=> erreur l'ors de l'insertion");
      }
    },
    DefinedSeed:function(seed)
    {
      this.wallet.CallHelpers_DefinedSeed(seed);
      if(this.save()==1)
        return "ok";
    },
    CreateWalletAccount:function(name_account)
    {
      if(this.wallet.CallHelpers_CreateWallet(name_account)=="ok")
      {
        if(this.save()==1)
          return "ok";
      }
    }
  }
});



export default UserWallet;
















/**/
