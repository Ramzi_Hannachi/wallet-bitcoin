import { Class } from 'meteor/jagi:astronomy';

const SendTransactionsHistory = Class.create({
  name:'SendTransactionsHistory',
  fields:
  {
    transactionId:String,
    transactionName:String,
    createdAt:Date,
    destinationAddress:String,
    SendSatoshis:Number,
    minerFee:Number,
    addressChange:String,
    sourceArrayAddress:[String]
  }
});


export default SendTransactionsHistory;














/**/
