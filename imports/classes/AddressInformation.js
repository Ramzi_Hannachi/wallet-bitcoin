import { Class } from 'meteor/jagi:astronomy';

const AddressInformation = Class.create({
  name:'AddressInformation',
  fields:
  {
    balance:Number,
    totalReceived:Number,
    totalSent:Number,
    unconfirmedBalance:Number
  }
});
export default AddressInformation;
