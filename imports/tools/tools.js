
var bitcore = require('bitcore-lib');

SatoshisToUSD=function(satoshis)
{
  var bitcoin=SatoshisToBTC(satoshis);
  var unit = new bitcore.Unit.fromBTC(bitcoin);
  var rate = 2824.08; // USD/BTC exchange rate
  var dollar = unit.atRate(rate);
  return dollar;
}
SatoshisToEuro=function(satoshis)
{
  var bitcoin=SatoshisToBTC(satoshis);
  var unit = new bitcore.Unit.fromBTC(bitcoin);
  var rate = 2508.926792; // EUR/BTC exchange rate
  var euro = unit.atRate(rate);
  return euro;
}
SatoshisToBTC=function(satoshis)
{
  var unitPreference = bitcore.Unit.BTC;
  return bitcore.Unit.fromSatoshis(satoshis).to(unitPreference);
}
BtcToSatoshis=function(bitcoin)
{
  return bitcore.Unit.fromBTC(bitcoin).toSatoshis();
}
MinerFee=function()
{
  return bitcore.Unit.fromMilis(0.5).toSatoshis();//0.128
}
