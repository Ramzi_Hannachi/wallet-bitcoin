import { Enum } from 'meteor/jagi:astronomy';

const Cointype = Enum.create({
  name:'Cointype',
  identifiers: ['Bitcoin','BitcoinTestnet']
});

export default Cointype;
