import { Enum } from 'meteor/jagi:astronomy';

const Exchange = Enum.create({
  name:'Exchange',
  identifiers: ['external','internal']
});

export default Exchange;
