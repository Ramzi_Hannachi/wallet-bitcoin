'use strict';
import UserWallet from '../imports/classes/UserWallet';

Meteor.publish("userwalletprofile", function(id)
{
  return UserWallet.find({'user_id':id});
});
