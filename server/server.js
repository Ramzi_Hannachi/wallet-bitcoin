'use strict';
import UserWallet from '../imports/classes/UserWallet';
import Exchange from '../imports/Enumeration/Exchange';
import Tools from '../imports/tools/tools';

var bitcore = require('bitcore-lib');
var Insight = require("bitcore-explorers").Insight;
var insight = new Insight("testnet");
var lupus = require('lupus');
var insight = new Insight("testnet");
var Promise = require("bluebird");
var bitcoinaddress = require('bitcoin-address');



UserWallet.extend({
  meteorMethods:
  {
    API_getANDsave_UnspentUtxos:function()
    {
      /*
      update the balance of accounts[] for user,
      update the utxos of accounts[].Address[].utxos[] for user
      */
      if(this.wallet)
      {
        //console.log("this:",this);
        var accounts=this.wallet.accounts;
        var sync_getUnspentUtxos = Meteor.wrapAsync(insight_getUnspentUtxos);
        for (var i = 0; i < accounts.length; i++)
        {
          var arrayAddress=accounts[i].arrayAddress;
          var result=sync_getUnspentUtxos(arrayAddress);
          if((result.message)=="ko")
          {
            return {message:result.message,error:result.error};
          }else{
            result=result.result;
            //console.log("resul unspentUtxos:",result.unspentUtxos);
            //console.log("resul balanceAccount:",result.balanceAccount);
            accounts[i].set('balance',result.balanceAccount);
            for (var j = 0; j < result.unspentUtxos.length; j++)
            {
              var address=result.unspentUtxos[j].address;
              var utxos=result.unspentUtxos[j].utxos;
              for (var k = 0; k < accounts[i].Address.length; k++)
              {
                if(accounts[i].Address[k].address==address)
                {
                  //console.log("utxos",utxos);
                  accounts[i].Address[k].set('utxos',utxos);
                  break;
                }
              }
            }
          }
        }
        if(this.save())
        {
          //1
          return {message:"ok",result:"Please consult your accounts"};
        }else {
          //0
          return {message:"o",result:"o"};
        }
      }
      //console.log("API_getANDsave_UnspentUtxos => save:",this.save());
    },
    API_send_Bitcoin:function(infromations)
    {
      var Account = this.wallet.accounts[infromations.numAccount];

      var sourceArrayAddress=[];
      var sourceArrayPrivateKey=[];

      var destinationAddress = infromations.destinationAddress;
      var SendSatoshis = BtcToSatoshis(infromations.sendSatoshis);
      var seed = infromations.seed;
      var minerFee = MinerFee();
      var addressChange=this.wallet.CallHelpers_CreateInternalAddress(infromations.numAccount);
      //console.log("addressChange:",addressChange);
      var sync_getUnspentUtxos = Meteor.wrapAsync(insight_getUnspentUtxos);
      var result=sync_getUnspentUtxos(Account.arrayAddress);
      //console.log("send: ",SendSatoshis+minerFee);
      if(result.message=="ko")
      {
        return {message:result.message,error:result.error};
      }else
      {
        result=result.result;
        if(result.balanceAccount<(SendSatoshis+minerFee))
        {
          return {message:"ko",error:"Please reload your account"};
        }else
        {
          //console.log(result);
          var unspentUtxos=result.unspentUtxos;
          for (var i = 0; i < unspentUtxos.length; i++)
          {
            var balanceAddress=unspentUtxos[i].balanceAddress;
            /**/
            var address=unspentUtxos[i].address;
            var privateKey=Account.CAllHelpers_GeneratePrivateKeyFromAddress(seed,address);
            /**/
            sourceArrayAddress.push(address);
            sourceArrayPrivateKey.push(privateKey);
            /**/
            //console.log("address: "+address+" balanceAddress:"+balanceAddress);
            if(balanceAddress>=SendSatoshis+minerFee)
            {
              //console.log("i:",i);
              break;
            }
          }
        /*console.log(ArrayAddress);
        console.log(sourceArrayPrivateKey);*/
        var informations={
          destinationAddress:destinationAddress,
          SendSatoshis:SendSatoshis,
          minerFee:minerFee,
          addressChange:addressChange,
          sourceArrayAddress:sourceArrayAddress,
          sourceArrayPrivateKey:sourceArrayPrivateKey
        };
        console.log(informations);

        var sync_insight_transaction = Meteor.wrapAsync(insight_transaction);
        var result=sync_insight_transaction(informations);
        if(result.message=="ko")
        {
          return {message:result.message,error:result.error};
        }else
        {
          var transactionId=result.result;
          var sendTransactionsHistory={
            transactionId:transactionId,
            transactionName:infromations.transactionName,
            createdAt:infromations.createdAt,
            destinationAddress:destinationAddress,
            SendSatoshis:SendSatoshis,
            minerFee:minerFee,
            addressChange:addressChange,
            sourceArrayAddress:sourceArrayAddress
          };
          //console.log(sendTransactionsHistory);
          //console.log("result:",transactionId);
          //console.log("save:",this.save());
          this.wallet.accounts[infromations.numAccount].SendTransactionsHistory.push(sendTransactionsHistory);
          this.wallet.CallHelpers_CreateExternalAddress(infromations.numAccount,"default");
          if(this.save())
          {
            return {message:"ok",result:"success! Id Transaction:"+transactionId};
          }
        }
      }
    }
  },
  API_get_AddressInfo:function(address,numAccount)
  {
    /*get and return AddressInfo for address, */
    var sync_getAddressInformations = Meteor.wrapAsync(insight_getAddressInformations);
    var addressInfo=sync_getAddressInformations(address);
    //console.log(AddressInfo);
    var addressInformation={
      balance:addressInfo.balance,
      totalReceived:addressInfo.totalReceived,
      totalSent:addressInfo.totalSent,
      unconfirmedBalance:addressInfo.unconfirmedBalance
    }
    //console.log("addressInformation:",addressInformation);
    var Address=this.wallet.accounts[numAccount].Address;
    var _index;
    for (var i = 0; i < Address.length; i++)
    {
      if(Address[i].address==address)
      {
        _index=i;
        break;
      }
    }
    this.wallet.accounts[numAccount].Address[_index].set('addressInformation',addressInformation);
    return this.save();
  }
  /**/
}
});

Meteor.methods
({

});

var insight_transaction=function(informations,callback)
{
  var destinationAddress=informations.destinationAddress;
  var SendSatoshis=informations.SendSatoshis;
  var minerFee=informations.minerFee;
  var addressChange=informations.addressChange;
  var sourceArrayAddress=informations.sourceArrayAddress;
  var sourceArrayPrivateKey=informations.sourceArrayPrivateKey;

  insight.getUnspentUtxos(sourceArrayAddress, function(error, utxos)
  {
    if(error)
  	{
      console.log(error);
  		callback(null,{message:"ko",error:error});
  	}else
  	{
  		var tx = new bitcore.Transaction();
  	  tx.from(utxos);
  	  tx.to(destinationAddress, SendSatoshis);
  	  tx.change(addressChange);
  	  tx.fee(minerFee);// 10000 Stashi par Defaut !!!!!
  	  tx.sign(sourceArrayPrivateKey);
      if (tx.getSerializationError())
      {
        let error = tx.getSerializationError().message;
        console.log(error);
        switch (error)
        {
          case 'Some inputs have not been fully signed':
            console.log('Please check your private key');
            callback(null,{message:"ko",error:"Please check your private key"});
            break;
          default:
            callback(null,{message:"ko",error:error});
        }
      }
  	  tx.serialize();
  	  insight.broadcast(tx, function(error, transactionId)
  	  {
        if (error)
        {
          console.log(error);
          callback(null,{message:"ko",error:error});
  	    }else
  	    {
          callback(null,{message:"ok",result:transactionId});
  	    }
  	  });
  	}
  });
}


/*SyncedCron.add({
  name: 'startFonction',
  schedule: function(parser)
  {
    return parser.text('every 5 seconds');
  },
  job: function()
  {
    console.log(user_id);
  }
});
SyncedCron.start();*/



var insight_getAddressInformations=function(address,callback)
{
  insight.address(address, function(error,AddressInfo)
  {
    if(error)
    {
      console.log(error);
      callback(null, error);
    }
    if(AddressInfo)
    {
      callback(null, AddressInfo);
    }
  });
}

/**/

var insight_getUnspentUtxos=function(arrayAddress,callback)
{
    //console.log("insight_getUnspentUtxos",arrayAddress);
    insight.getUnspentUtxos(arrayAddress, function(error, _utxos)
    {
      if(error){
        console.log("error",error);
        callback(null,{message:"ko",error:error});
      }
      else{
        var unspentUtxos=[];
        var balanceAccount=0;
        lupus(0, _utxos.length, function(i)
        {
          //console.log("_utxos",_utxos);
          var txId=_utxos[i]['txId'];
          var outputIndex=_utxos[i]['outputIndex'];
          var script=_utxos[i]['script'];
          var address=_utxos[i]['address'].toString();
          var satoshis=_utxos[i]['satoshis'];
          balanceAccount+=satoshis;

          var utxo={
            txId:txId,
            //createdAt:new Date(),
            outputIndex:outputIndex,
            //script:script,
            satoshis:satoshis
          };
          if(item=Containes(unspentUtxos,address))
          {
            item.balanceAddress+=satoshis;
            item.utxos.push(utxo);
          }else
          {
            var item={
              address:address,
              balanceAddress:satoshis,
              utxos:[]
            };
            item.utxos.push(utxo);
            //console.log("utxos:",item.utxos);
            unspentUtxos.push(item);
          }
        },function()
        {
          var result={
            balanceAccount:balanceAccount,
            unspentUtxos:unspentUtxos
          }
          //console.log("insight_getUnspentUtxos result:",result);
          callback(null,{message:"ok",result:result});
        });
      }
    });
}

var Containes=function(transactions,address)
{
  for(var i = 0; i < transactions.length; i++)
  {
    if (transactions[i].address == address)
    {
      return transactions[i];//item
    }
  }
  return null;
}
