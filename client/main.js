import UserWallet from '../imports/classes/UserWallet';
import Wallet from '../imports/classes/Wallet';
import Address from '../imports/classes/Address';
import Account from '../imports/classes/Account';
import Utxos from '../imports/classes/Utxos';
import SendTransactionsHistory from '../imports/classes/SendTransactionsHistory';
import AddressInformation from '../imports/classes/AddressInformation';
import Tools from '../imports/tools/tools';

if(Meteor.isClient)
{
  window.UserWallet = UserWallet;
  window.Wallet = Wallet;
  window.Address = Address;
  window.Account = Account;
  window.Utxos = Utxos;
  window.SendTransactionsHistory = SendTransactionsHistory;
}



Meteor.startup(function()
{
  CALL_API_getANDsave_UnspentUtxos();

  sAlert.config({
          effect: 'slide',
          position: 'top-right',
          timeout: 5000,
          html: false,
          onRouteClose: true,
          stack: true,
          // or you can pass an object:
          // stack: {
          //     spacing: 10 // in px
          //     limit: 3 // when fourth alert appears all previous ones are cleared
          // }
          offset: 0, // in px - will be added to first alert (bottom or top - depends of the position in config)
          beep: false,
          // examples:
          // beep: '/beep.mp3'  // or you can pass an object:
          // beep: {
          //     info: '/beep-info.mp3',
          //     error: '/beep-error.mp3',
          //     success: '/beep-success.mp3',
          //     warning: '/beep-warning.mp3'
          // }
          onClose: _.noop //
          // examples:
          // onClose: function() {
          //     /* Code here will be executed once the alert closes. */
          // }
      });
});
