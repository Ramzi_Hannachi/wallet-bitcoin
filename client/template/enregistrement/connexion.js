Template.register.events({
  "click #submit": function(event, template)
  {
    event.preventDefault();
    var username = $("#username_register").val();
    var email = $("#email_register").val();
    var password = $("#password_register").val();
    var profile = {};

		var user = {
			username: username,
			email: email,
			password: password,
			profile: profile
		};
    new UserWallet().RegisterUser(user,function(error,result)
    {
      if(error)
        sAlert.error('Error:'+error.reason);
      if(result=="ok")
        Router.go('login');
    });
  }
});

Template.login.events({
  "click #submit": function(event, template)
  {
    event.preventDefault();
    var email = $("#email_login").val();
    var password = $("#password_login").val();

    Meteor.loginWithPassword(email , password , function(error)
    {
      if(error)
        sAlert.error('Error:'+error.reason);
      else
        Router.go('accounts');
    });
  }
});
