
Template.formSeed.events(
  {
    "click #submit": function(event, template)
    {
      event.preventDefault();

      var seed =  $("#seed").val();

      if(UserWallet.findOne().DefinedSeed(seed)=="ok")
      {
        $("#seed").val('');
        sAlert.success('Success!');
      }else
      {
        sAlert.error('Error');
      }


    }
});
