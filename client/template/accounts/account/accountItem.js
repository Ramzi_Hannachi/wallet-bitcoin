import Exchange from '../../../../imports/Enumeration/Exchange';

Template.formCreateExternalAddress.events({
  "click #submit": function(event, template)
  {
    event.preventDefault();
    var tag=$("#tag_"+this.numAccount).val();
    if(result=UserWallet.findOne().CreateExternalAddress(this.numAccount,tag))
    {
      $("#tag").val('');
      sAlert.success('Success!');
    }else {
      sAlert.error('Error');
    }
    template.$('#close').click();
  }
});

Template.ReceiveCryptocurrency.helpers({
  GetPaymentAddress:function()
  {
    //console.log("this:",this);
    return this.Account_getPaymentAddress();
  }
});
/*
Template.accountItem.helpers({
  Address_GetAdrressInformations:function(address)
  {
    var result = ReactiveMethod.call('API_get_AddressInfo',address);
    //console.log(result);
    return result;
  }
});*/
Template.formSendCryptocurrency.events({
  "click button": function(event, template)
  {
    event.preventDefault();
    var destinationAddress=$("#destinationAddress_"+this.numAccount).val();
    var sendSatoshis=$("#sendSatoshis_"+this.numAccount).val();
    var seed=$("#seed_"+this.numAccount).val();
    var transactionName=$("#transactionName_"+this.numAccount).val();
    var informations={
      numAccount:this.numAccount,
      destinationAddress:destinationAddress,
      sendSatoshis:sendSatoshis,
      seed:seed,
      transactionName:transactionName,
      createdAt:new Date()
    };

    var collapse_loading=template.$("#loading").hasClass('collapse in');
    console.log(collapse_loading);
    if(collapse_loading==false)
    {
      template.$("button").prop('disabled', true);
      template.$("#loading").collapse('show');
      UserWallet.findOne().callMethod('API_send_Bitcoin',informations,function(error,result)
      {
        template.$("button").prop('disabled', false);
        template.$("#loading").collapse('hide');

        console.log("result",result);
        if(result.message=="ok")
        {
          sAlert.success(''+result.result);
        }else
        {
          sAlert.error(''+result.error);
        }
      });
    }

  }
});

Template.TransactionHistoryReceived.onRendered(function()
{
  Session.set('ok',true);
});
Template.TransactionHistoryReceived.events({
  "click #button_AddressInformation": function(event, template){
    var address=this.address;
    var numAccount=this.path.slice(9,10);//data-toggle="collapse" data-target="#addressInformation_{{address}}"

    var collapse_addressInformation=template.$("#addressInformation_"+address).hasClass('collapse in');
    //console.log("xxx:",$("#addressInformation_"+address).hasClass('collapse in'));

    if(collapse_addressInformation==false)
    {
      if(Session.get('ok'))
      {
        Session.set('ok',false);
        console.log("click");
        template.$("#loading_"+address).collapse('show');
        UserWallet.findOne().callMethod('API_get_AddressInfo',address,numAccount,function(err,result)
        {
          //console.log(result);
          if(result==0||result==1)
          {
            setTimeout(function()
            {
              Session.set('ok',true);
              template.$("#loading_"+address).collapse('hide');
              template.$("#addressInformation_"+address).collapse('show');
            }, 300);
          }
        });
      }
    }else {
      template.$("#loading_"+address).collapse('hide');
      template.$("#addressInformation_"+address).collapse('hide');
    }

    /*setTimeout(function()
    {
      $("#addressInformation_"+address).collapse('hide');
    }, 3000);*/
  }
});













/**/
