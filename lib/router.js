import UserWallet from '../imports/classes/UserWallet';

Router.configure({
  layoutTemplate:"layout",
  loadingTemplate:"loading",
  waitOn:function()
  {
    return Meteor.subscribe("userwalletprofile",Meteor.userId());// Meteor.userId()
  }
});

Router.route('/',
{
  name : 'accounts',
  data:function()
  {
    var userwallet = UserWallet.findOne();
    //console.log(userwallet);
    return {
      userwallet:userwallet
    };
  }
});
/*Router.route("/formAccount",
{
  name:'formAccount'
});*/
Router.route('/login',
{
  name:'login'
});
Router.route('/register',
{
  name:'register'
});
/************/
var requireLogin = function()
{
  if(!Meteor.userId())
  {
    Router.go("login");
  }else {
    this.next();
  }
};
var notAccessLogin = function()
{
  if(Meteor.userId())
  {
    Router.go("accounts");
  }else {
    this.next();
  }
};
/************/
Router.onBeforeAction(requireLogin,{only:'accounts'});
//Router.onBeforeAction(requireLogin,{only:'formAccount'});

Router.onBeforeAction(notAccessLogin,{only:'login'});
Router.onBeforeAction(notAccessLogin,{only:'register'});














/**/
